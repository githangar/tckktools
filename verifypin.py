from smartcard.System import readers

# Connect to the card
r = readers()
connection = r[0].createConnection()
connection.connect()

pin = input("PIN: ")

cmd = [0, 0x20, 0, 0x01, len(pin)]

for pinchar in pin:
    cmd.append(ord(pinchar))

resp, cl1, cl2 = connection.transmit(cmd)
if (cl1 == 0x90 and cl2 == 0x00):
    print("Verified PIN successfully.")
else:
    print(f"Couldn't verify PIN. Got {repr(resp)} with status {cl1:x}{cl2:x}.")

